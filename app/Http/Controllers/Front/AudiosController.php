<?php

namespace ckeditor\Http\Controllers\Front;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;

class AudiosController extends Controller
{
    /**
     * return Audio Page
     * @params : No params
     */
    public function index()
    {
        return view('front.pages.audio');

    }
}
