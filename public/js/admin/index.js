/**
 * After compose content, it is saved to database
 *
 * Method : Using ajax to send content to server if user click submit button
 * @params : Content and token
 */
function saveData(token) {
    $('#sub_dash').click(function() {
        // @sET header attach a csrf token to verify user on serve side
        var type = 'POST';
        var urls = "/admin/saveContent";
        var formData = {
            content: CKEDITOR.instances.editor1.getData(),
            _token: token
        };
        $.ajax({
            url: urls,
            type: type,
            data: formData,
            dataType: 'json',
            success: function(data) {
                var content = data.content;
                $('#show').html(content);
            }
        });
    });
}

/***
 *  Check blank character in content: server side
 *  If content have more two continued space characters then show message
 *  warning user to change it.
 *
 * @param token
 */
function accessibilityCheck(token) {
    $('#check_access').click(function() {
        // @sET header attach a csrf token to verify user on serve side
        var type = 'POST';
        var urls = "/admin/checkAccessibility";
        var formData = {
            content: CKEDITOR.instances.editor1.getData(),
            _token: token
        };
        $.ajaxSetup({
           headers:{
               'X-CSRF-TOKEN': token
           }
        });
        $.ajax({
            url: urls,
            type: type,
            data: formData,
            dataType: 'json',
            success: function(data) {
                var newContent = data.newContent;

                $('#show').html(newContent);
            }
        });
    });
}


/**
 * Show popup message warning about have many consecutive whitespaces in contents!
 * And warning fixing this error
 *
 * Event: when on mouse over label tag
 */
function cxPopUpOpen(tags) {
    setTimeout(function() {
        alert("Cảnh báo : Nội dung soạn thảo có thể đang chứ 2 hoặc nhiều kí tự trắng liên tiếp. " + "\n Vui lòng chọn chức năng [Sửa nhanh] để khắc phục lỗi trên");
    }, 1000);

}

/**
 * Fix error
 * After receive warning message, then to fix error consecutive whitespaces in
 * Method: Check 'Checked Input' to decide remove consecutive whitespaces. If
 * agree then replace group whitespaces to one whitespace.
 * new content to set in CKeditor
 *
 * @params : Content and token
 */
function removeWSP(thisTag) {
    $('#show input').each(function() {
        var hasCheck = $(this).is(':checked');
        if (hasCheck) {
            var className = $(this).attr('class');

            //  Todo replace consecutive whitespace by 1 space and remove It's Input, label
            var temp = " ";
            $(this).after(temp);
            $("." + className).remove();
        } else {
            var className = $(this).attr('class');
            var temp = $("label." + className ).attr('data-url');
            $(this).after(temp);
            $("." + className).remove();
        }
    });
    var newContent = $('#show').html();
    CKEDITOR.instances.editor1.setData(newContent);
    $('#show').empty();
}

/**
 * Add Input
 * Add a input when user add a number in array to sort
 */
function addInput() {
    $('#array input').last().after('&nbsp;<input class="inputArray" type="text" name="arr[]">');
}

/**
 * Clear Array
 * Remove list input and reset default value
 */
function clearArray() {
        $('#array').html('                        <input class="inputArray" type="text" autofocus>\n' +
            '                        <input class="inputArray" type="text">\n' +
            '                        <button type="button" class="btn btn-sm btn-info " id="addInput" onclick="addInput()">Thêm</button>\n' +
            '                        <button type="button" class="btn btn-sm btn-default " id="clearArray" onclick="clearArray()">Clear</button>');
        $('#newArr').html('');
        $('#time').html('');
}

/**
 * Get value from list input and push in array send to server to sort
 * Remove elements have empty value, without number
 *
 * @params : value input
 * @output : Array
 */
function getArrayInput() {
    var signal = 1;
    var arr = [];
    $('.inputArray').each(function(){
        var val = $(this).val();
        var reg = /[^0-9-]+|[0-9]+-|\s+|^$/;

        if(reg.test(val) === true) {
            signal = 0;
        } else {
            arr.push(val);
        }
    });
    if (signal === 0) {
        alert("Giá trị không hợp lệ, hoặc không bỏ trống ô nhập vào!");
        return [];
    } else {
        return arr;
    }
}

/**
 * Sort: Send data to server to sort
 *
 * @params : Array, Method type and order sort :decs or asc
 */

function sort(token) {
    var arr = getArrayInput();
    if (arr.length) {
        var orderSort = $("input[name='orderby']:checked").val();
        var typeSort = $("input[name='typeSort']:checked").val();

        var type = 'POST';
        var urls = "/admin/sortIndex";
        var formData = {
            arr: arr,
            orderSort: orderSort,
            typeSort: typeSort,
            _token: token
        };


        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': token
            }
        });
        $.ajax({
            url: urls,
            type: type,
            data: formData,
            dataType: 'json',
            success: function(data) {
                var signal = data.signal;
                if (!signal) {
                    var message = data.message;
                    alert(message);
                } else {
                    var result = data.data;
                    var newArr = result.arr;
                    var time = result.time;

                    $('#time').html(time);

                    var len = newArr.length;
                    var seriElements = '';
                    for (var i = 0; i < len; i++) {
                        seriElements += newArr[i] + ' &nbsp; &nbsp; ';
                    }
                    $('#newArr').html(seriElements);
                }
            }
        });

    }
}





